package com.time.beans;

import java.util.List;

/**
 * 
 * This is bean class is used to hold the different value of time range
 * for example start hr, start min, start sec, end hr, end min, end sec
 * @author 510963
 * 
 */

public class TimeBean {

	private int startHr;
	private int startMin;
	private int startSec;
	private int endHr;
	private int endMin;
	private int endSec;

	public TimeBean(List<Integer> listStartTIme, List<Integer> listEndTIme) {

		startHr = listStartTIme.get(0);
		startMin = listStartTIme.get(1);
		startSec = listStartTIme.get(2);
		endHr = listEndTIme.get(0);
		endMin = listEndTIme.get(1);
		endSec = listEndTIme.get(2);
	}

	/** get start hr
	 * @return start hr
	 */
	public int getStartHr() {
		return startHr;
	}
	/** set start hr
	 * @param startHr
	 */
	public void setStartHr(int startHr) {
		this.startHr = startHr;
	}
	/** get start min
	 * @return start min
	 */
	public int getStartMin() {
		return startMin;
	}
	/** Set start min
	 * @param startMin
	 */
	public void setStartMin(int startMin) {
		this.startMin = startMin;
	}
	/** Get start Sec
	 * @return start sec
	 */
	public int getStartSec() {
		return startSec;
	}
	/** Set start sec
	 * @param startSec
	 */
	public void setStartSec(int startSec) {
		this.startSec = startSec;
	}
	/** Get end hr
	 * @return end hr
	 */
	public int getEndHr() {
		return endHr;
	}
	/** Set end hr
	 * @param endHr
	 */
	public void setEndHr(int endHr) {
		this.endHr = endHr;
	}
	/** Get end min
	 * @return end min
	 */
	public int getEndMin() {
		return endMin;
	}
	/** Set end min
	 * @param endMin
	 */
	public void setEndMin(int endMin) {
		this.endMin = endMin;
	}
	/** Get end sec
	 * @return end sec
	 */
	public int getEndSec() {
		return endSec;
	}
	/** Set end sec
	 * @param endSec
	 */
	public void setEndSec(int endSec) {
		this.endSec = endSec;
	}

	/**
	 * String representation of object
	 */
	@Override
	public String toString() {
		return "TimeBean [startHr=" + startHr + ", startMin=" + startMin + ", startSec=" + startSec + ", endHr=" + endHr
				+ ", endMin=" + endMin + ", endSec=" + endSec + "]";
	}
}